load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

ECLIPSE_GITLAB = "https://gitlab.eclipse.org/eclipse"

UNITS_TAG = "2.3.3"

def mantle_api_deps():
    """Load dependencies"""
    maybe(
        http_archive,
        name = "units_nhh",
        url = "https://github.com/nholthaus/units/archive/refs/tags/v{tag}.tar.gz".format(tag = UNITS_TAG),
        sha256 = "b1f3c1dd11afa2710a179563845ce79f13ebf0c8c090d6aa68465b18bd8bd5fc",
        strip_prefix = "./units-{tag}".format(tag = UNITS_TAG),
        build_file = "//bazel:units.BUILD",
    )
