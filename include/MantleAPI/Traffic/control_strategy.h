/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  control_strategy.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_CONTROL_STRATEGY_H
#define MANTLEAPI_TRAFFIC_CONTROL_STRATEGY_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/lane_definition.h>
#include <MantleAPI/Traffic/traffic_light_properties.h>

#include <vector>

namespace mantle_api
{

/// Movement domain of the control strategy, which specifies the desired movement behavior for the entity
enum class MovementDomain
{
  kUndefined = 0,
  kLateral,
  kLongitudinal,
  kBoth,
  kNone
};

/// Type of the control strategy
enum class ControlStrategyType
{
  kUndefined = 0,
  kKeepVelocity,
  kKeepLaneOffset,
  kFollowHeadingSpline,
  kFollowLateralOffsetSpline,
  kFollowVelocitySpline,
  kAcquireLaneOffset,
  kFollowTrajectory,
  kUpdateTrafficLightStates,
  kPerformLaneChange
};

/// Defintion of all control strategies for a single entity.
/// The runtime mapping of an private action instance to the simulator core.
struct ControlStrategy
{
  virtual ~ControlStrategy() = default;

  // TODO: extend by bool use_dynamic_constraints when needed (false assumed at the moment)

  MovementDomain movement_domain{MovementDomain::kUndefined};  ///< Movement domain of the control strategy
  ControlStrategyType type{ControlStrategyType::kUndefined};   ///< Type of the control strategy
};

/// @brief Equality comparison for ControlStrategy.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equal to the values of rhs.
constexpr bool operator==(const ControlStrategy& lhs, const ControlStrategy& rhs) noexcept
{
  return lhs.movement_domain == rhs.movement_domain && lhs.type == rhs.type;
}

/// @brief  Inequality comparison for ControlStrategy.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
constexpr bool operator!=(const ControlStrategy& lhs, const ControlStrategy& rhs) noexcept
{
  return !(lhs == rhs);
}

/// Controls an entity to keeps current velocity
struct KeepVelocityControlStrategy : public ControlStrategy
{
  KeepVelocityControlStrategy()
  {
    movement_domain = MovementDomain::kLongitudinal;
    type = ControlStrategyType::kKeepVelocity;
  }
  // Doesn't need configuration attributes. Controller keeps current velocity on adding entity or update
};

/// Controls an entity to keeps current lane offset
struct KeepLaneOffsetControlStrategy : public ControlStrategy
{
  KeepLaneOffsetControlStrategy()
  {
    movement_domain = MovementDomain::kLateral;
    type = ControlStrategyType::kKeepLaneOffset;
  }
  // Doesn't need configuration attributes. Controller keeps current lane offset on adding entity or update
};

/// Controls an entity to follow a heading angle from spline
struct FollowHeadingSplineControlStrategy : public ControlStrategy
{
  FollowHeadingSplineControlStrategy()
  {
    movement_domain = MovementDomain::kLateral;
    type = ControlStrategyType::kFollowHeadingSpline;
  }

  /// List of the heading angles of splines
  std::vector<mantle_api::SplineSection<units::angle::radian>> heading_splines;
  /// The default value of the heading angle
  units::angle::radian_t default_value{0};
};

/// Controls an entity to follow a velocity from spline
struct FollowVelocitySplineControlStrategy : public ControlStrategy
{
  FollowVelocitySplineControlStrategy()
  {
    movement_domain = MovementDomain::kLongitudinal;
    type = ControlStrategyType::kFollowVelocitySpline;
  }

  /// List of the velocities of splines
  std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines;
  /// The default value of the velocity
  units::velocity::meters_per_second_t default_value{0};
};

/// @brief Equality comparison for FollowVelocitySplineControlStrategy.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equal to the values of rhs.
constexpr bool operator==(const FollowVelocitySplineControlStrategy& lhs,
                          const FollowVelocitySplineControlStrategy& rhs) noexcept
{
  return lhs.default_value == rhs.default_value && lhs.velocity_splines == rhs.velocity_splines;
}

/// @brief  Inequality comparison for FollowVelocitySplineControlStrategy.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
constexpr bool operator!=(const FollowVelocitySplineControlStrategy& lhs,
                          const FollowVelocitySplineControlStrategy& rhs) noexcept
{
  return !(lhs == rhs);
}

/// Controls an entity to follow a lateral offset from spline
struct FollowLateralOffsetSplineControlStrategy : public ControlStrategy
{
  FollowLateralOffsetSplineControlStrategy()
  {
    movement_domain = MovementDomain::kLateral;
    type = ControlStrategyType::kFollowLateralOffsetSpline;
  }

  /// List of the lateral offsets of splines
  std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offset_splines;
};

/// Defines how a target value will be acquired (with a constant rate, in a defined distance, within a defined time).
enum class Dimension
{
  kUndefined = 0,
  kDistance,
  kRate,
  kTime
};

/// Function type used to represent the change of a given variable over time or distance.
enum class Shape
{
  kUndefined = 0,
  kStep,
  kCubic,
  kLinear,
  kSinusoidal
};

/// Specifies the dynamics of a value transition and defines how the value changes over time or distance.
struct TransitionDynamics
{
  /// Defines how a target value will be acquired
  Dimension dimension{Dimension::kUndefined};
  /// The shape of the transition function between current and target value
  Shape shape{Shape::kUndefined};
  /// The value for a predefined rate, time or distance to acquire the target value
  double value{0.0};
};

/// Controls the transition to a defined lane offset of an entity
struct AcquireLaneOffsetControlStrategy : public ControlStrategy
{
  AcquireLaneOffsetControlStrategy()
  {
    movement_domain = MovementDomain::kLateral;
    type = ControlStrategyType::kAcquireLaneOffset;
  }

  /// Lane offset
  units::length::meter_t offset{};
  /// Specifies the dynamics of a value transition
  TransitionDynamics transition_dynamics;
};

/// Controls the transition of a current light state to the target light state
struct TrafficLightStateControlStrategy : public ControlStrategy
{
  TrafficLightStateControlStrategy()
  {
    type = ControlStrategyType::kUpdateTrafficLightStates;
    movement_domain = MovementDomain::kNone;
  }

  /// List of specific traffic light phases
  std::vector<TrafficLightPhase> traffic_light_phases{};
  /// The "repeat_states" flag determines, if the light state is repeated or not
  bool repeat_states{false};
};

/// Definition of time value context as either absolute or relative
enum class ReferenceContext
{
  kAbsolute = 0,
  kRelative
};

/// Controls an entity to follow a trajectory
struct FollowTrajectoryControlStrategy : public ControlStrategy
{
  // TODO: Extend the FollowTrajectoryControlStrategy to support shapes like NURBS and clothoid

  /// Definition of the time information present in trajectory
  struct TrajectoryTimeReference
  {
    /// Specification of the time domain (absolute or relative)
    ReferenceContext domainAbsoluteRelative{ReferenceContext::kAbsolute};
    /// Scaling factor for time values. 
    /// While values smaller than 1.0 represent negative scaling, values larger than 1.0 will result in positive scaling. 
    /// A value of 1.0 means no scaling.
    double scale{1.0};
    /// Global offset for all time values
    units::time::second_t offset{0.0};
  };

  FollowTrajectoryControlStrategy()
  {
    movement_domain = MovementDomain::kBoth;
    type = ControlStrategyType::kFollowTrajectory;
  }

  /// Trajectory definition
  Trajectory trajectory;
  /// Time information provided within the trajectory
  std::optional<TrajectoryTimeReference> timeReference;
};

/// Controls an entity to perform a lane change.
/// Describes the transition between an entity's current lane and its target lane.
struct PerformLaneChangeControlStrategy : public ControlStrategy
{
  PerformLaneChangeControlStrategy()
  {
    movement_domain = MovementDomain::kLateral;
    type = ControlStrategyType::kPerformLaneChange;
  }

  /// ID of the target lane the entity will change to
  mantle_api::LaneId target_lane_id{0};
  /// Lane offset to be reached at the target lane (the action will end there)
  units::length::meter_t target_lane_offset{0.0};
  /// Shape/time of lane change action
  TransitionDynamics transition_dynamics{};
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_CONTROL_STRATEGY_H
