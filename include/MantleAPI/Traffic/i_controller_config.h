/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_controller_config.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_CONTROLLER_CONFIG_H
#define MANTLEAPI_TRAFFIC_I_CONTROLLER_CONFIG_H

#include <MantleAPI/Common/route_definition.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <MantleAPI/Traffic/control_strategy.h>

#include <algorithm>
#include <map>
#include <memory>
#include <vector>

namespace mantle_api
{

/// Base definition for creating a new controller for an entity
struct IControllerConfig
{
  /// @brief default destructor
  virtual ~IControllerConfig() = default;
  /// @brief Name of the controller. Might be ignored by the environment.
  std::string name;
  /// Pointer to the map query service
  /// @todo: Check why map_query_service is part of the interface because it is not set from engine side but only in the environment on calling AddController()
  ILaneLocationQueryService* map_query_service{nullptr};
  /// List of active control strategies
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
  /// Specifies the route behavior for the control stratgies
  RouteDefinition route_definition;
};

/// @brief Equality comparison for IControllerConfig.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]  rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs and rhs are equal
inline bool operator==(const IControllerConfig& lhs, const IControllerConfig& rhs) noexcept
{
  if (&lhs == &rhs)
  {
    return true;
  }

  const auto compare_control_strategies = [](const auto& lhs, const auto& rhs)
  {
    if (lhs.size() != rhs.size())
    {
      return false;
    }
    else
    {
      for (size_t i = 0; i < lhs.size(); i++)
      {
        if (*(lhs[i]) != *(rhs[i]))
        {
          return false;
        }
      }
    }
    return true;
  };

  const auto compare_route_definition = [](const auto& lhs, const auto& rhs)
  {
    if (lhs.waypoints.size() != rhs.waypoints.size())
    {
      return false;
    }

    for (size_t i = 0; i < lhs.waypoints.size(); ++i)
    {
      if (lhs.waypoints[i].route_strategy != rhs.waypoints[i].route_strategy ||
          lhs.waypoints[i].waypoint != rhs.waypoints[i].waypoint)
      {
        return false;
      }
    }
    return true;
  };

  return lhs.name == rhs.name &&
         lhs.map_query_service == rhs.map_query_service &&
         compare_control_strategies(lhs.control_strategies, rhs.control_strategies) &&
         compare_route_definition(lhs.route_definition, rhs.route_definition);
}

/// @brief Check for not equal
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]  rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs and rhs are not equal
inline bool operator!=(const IControllerConfig& lhs, const IControllerConfig& rhs) noexcept
{
  return !(lhs == rhs);
}

/// Define a "No Operation" controller
struct NoOpControllerConfig : public IControllerConfig
{
};

/// Define a default or internal controller (usually one per entity)
struct InternalControllerConfig : public IControllerConfig
{
};

/// Define an external controller with custom parameters
struct ExternalControllerConfig : public IControllerConfig
{
  /// Additional parameters as name value pair
  std::map<std::string, std::string> parameters;
};

/// @brief Check for equality
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]  rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs and rhs are equal
inline bool operator==(const ExternalControllerConfig& lhs, const ExternalControllerConfig& rhs) noexcept
{
  auto compare_base = [](const IControllerConfig& lhs, const IControllerConfig& rhs)
  {
    return lhs == rhs;
  };

  return compare_base(lhs, rhs) && lhs.parameters == rhs.parameters;
}

/// @brief Check for not equal
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]  rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs and rhs are not equal
inline bool operator!=(const ExternalControllerConfig& lhs, const ExternalControllerConfig& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_CONTROLLER_CONFIG_H
