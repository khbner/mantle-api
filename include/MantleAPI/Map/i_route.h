/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_route.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_I_ROUTE_H
#define MANTLEAPI_MAP_I_ROUTE_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/lane_definition.h>
#include <units.h>

namespace mantle_api
{

/// Base interface for route
/// It represents continuous path throughout the road network, defined by a series of waypoints
class IRoute : public virtual IIdentifiable
{
public:
  /// Adds a waypoint to the route
  ///
  /// @param inert_pos Waypoint to add
  /// @return created route defined by a series of waypoints
  virtual IRoute& AddWaypoint(const Vec3<units::length::meter_t>& inert_pos) = 0;

  /// Adds a waypoint to the route
  ///
  /// @param inert_pos Waypoint to add
  /// @return created route defined by a series of waypoints
  virtual IRoute& AddWaypoint(Vec3<units::length::meter_t>&& inert_pos) = 0;

  /// Returns inertial position from track position
  ///
  /// @param route_pos    s coordinate on lane
  /// @param lane_id      ID of lane
  /// @param lane_offset  t coordinate on lane
  /// @return position in Cartesian coordinate system
  [[nodiscard]] virtual Vec3<units::length::meter_t> GetInertPos(units::length::meter_t route_pos,
                                                                 LaneId lane_id,
                                                                 units::length::meter_t lane_offset = units::length::meter_t{
                                                                     0.0}) const = 0;
  
  /// Returns interpolated value for the width of the lane at the given position
  ///
  /// @param lane_id    ID of lane to search in
  /// @param route_pos  s coordinate of search start
  /// @return width at position
  [[nodiscard]] virtual units::length::meter_t GetLaneWidth(units::length::meter_t route_pos, LaneId lane_id) const = 0;
  
  /// Returns ID of the lane that encloses the passed in position within its shape
  ///
  /// @param inert_pos  Position to search for the Lane ID
  /// @return ID of the lane
  [[nodiscard]] virtual LaneId GetLaneId(const Vec3<units::length::meter_t>& inert_pos) const = 0;

  /// Returns distance from start to the given position along the set route
  ///
  /// @param inert_pos End position
  /// @return distance from start to the given position
  [[nodiscard]] virtual units::length::meter_t GetDistanceFromStartTo(const Vec3<units::length::meter_t>& inert_pos) const = 0;

  /// Returns the length of the route
  ///
  /// @return length of the route
  [[nodiscard]] virtual units::length::meter_t GetLength() const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_MAP_I_ROUTE_H
