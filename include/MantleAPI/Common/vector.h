/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  vector.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_VECTOR_H
#define MANTLEAPI_COMMON_VECTOR_H

#include <MantleAPI/Common/floating_point_helper.h>

#include <cstdint>

namespace mantle_api
{

template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>

/// 3-dimensional vector that contains three components (x, y, z) in the right-handed Cartesian coordinate system
struct Vec3
{
  Vec3() = default;

  /// Constructor
  ///
  /// @param[in] x_in  x-value
  /// @param[in] y_in  y-value
  /// @param[in] z_in  z-value
  Vec3(T x_in, T y_in, T z_in)
      : x{x_in}, y{y_in}, z{z_in}
  {
  }

  T x{0}; ///< x-component
  T y{0}; ///< y-component
  T z{0}; ///< z-component

  /// @brief Returns length of the vector
  ///
  /// @returns length of the vector
  inline T Length() const { return units::math::sqrt((x * x) + (y * y) + (z * z)); }

  /// @brief Changes the sign of the 3d vector
  ///
  /// @returns 3d vector
  inline Vec3<T> operator-() const noexcept
  {
    return {-x, -y, -z};
  }
};

/// @brief  almost-equality
/// @details  Compares the values of two 3d vectors.
/// @tparam T the value type
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs almost equal to the values of rhs.
template <typename T>
constexpr bool operator==(const Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  return AlmostEqual(lhs.x, rhs.x) && AlmostEqual(lhs.y, rhs.y) && AlmostEqual(lhs.z, rhs.z);
}

/// @brief  inequality
/// @details  Compares the value of two 3d vectors.
/// @tparam T the value type
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
template <typename T>
constexpr bool operator!=(const Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  return !(lhs == rhs);
}

/// @brief subtraction
/// @details Returns the difference of two 3d vectors
/// @tparam T the value type
/// @param[in]  lhs left-hand side value for the difference
/// @param[in]	rhs right-hand side value for the difference
/// @returns difference of lhs and rhs.
template <typename T>
constexpr Vec3<T> operator-(const Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  return {lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z};
}

/// @brief addition
/// @details Returns the sum of two 3d vectors
/// @tparam T the value type
/// @param[in]  lhs left-hand side value for the sum
/// @param[in]	rhs right-hand side value for the sum
/// @returns sum of lhs and rhs.
template <typename T>
constexpr Vec3<T> operator+(const Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  return {lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z};
}

/// @brief multiplication
/// @details Multiplication by a scalar for 3d vector
/// @tparam T the value type
/// @param[in]  lhs left-hand side value for the multiplication
/// @param[in]	d   scalar
/// @returns the scalar multiplied vector
template <typename T>
constexpr Vec3<T> operator*(const Vec3<T>& lhs, double d) noexcept
{
  return {lhs.x * d, lhs.y * d, lhs.z * d};
}

/// @brief multiplication
/// @details Multiplication by a scalar for 3d vector
/// @tparam T the value type
/// @param[in]	d   scalar
/// @param[in]	rhs right-hand side value for the multiplication
/// @returns the scalar multiplied vector
template <typename T>
constexpr Vec3<T> operator*(double d, const Vec3<T>& rhs) noexcept
{
  return rhs * d;
}

/// @brief division
/// @details Division by a scalar for 3d vector
/// @tparam T the value type
/// @param[in]  lhs left-hand side value for the division
/// @param[in]	d   scalar
/// @returns the lhs divided by d
template <typename T>
constexpr Vec3<T> operator/(const Vec3<T>& lhs, double d) noexcept
{
  return {lhs.x / d, lhs.y / d, lhs.z / d};
}

/// @brief addable
/// @details Add a 3d vector to a 3d vector
/// @tparam T the value type
/// @param[in]  lhs left-hand side value
/// @param[in]	rhs right-hand side value, which will be added
/// @returns the lhs where rhs is added
template <typename T>
constexpr Vec3<T> operator+=(Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  lhs.x += rhs.x;
  lhs.y += rhs.y;
  lhs.z += rhs.z;
  return lhs;
}

/// @brief subtractable
/// @details Subtract a 3d vector from a 3d vector
/// @tparam T the value type
/// @param[in]  lhs left-hand side value
/// @param[in]	rhs right-hand side value
/// @returns the lhs from where rhs is subtracted
template <typename T>
constexpr Vec3<T> operator-=(Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  lhs.x -= rhs.x;
  lhs.y -= rhs.y;
  lhs.z -= rhs.z;
  return lhs;
}

/// @brief addable
/// @details Add a scalar to a 3d vector
/// @tparam T the value type
/// @param[in]  lhs left-hand side value
/// @param[in]	d   scalar
/// @returns the lhs where d is added
template <typename T>
constexpr Vec3<T> operator+=(Vec3<T>& lhs, double d) noexcept
{
  lhs.x += d;
  lhs.y += d;
  lhs.z += d;
  return lhs;
}

/// @brief subtractable
/// @details Subtract a scalar from a 3d vector
/// @tparam     T   Type of vector components
/// @param[in]  lhs left-hand side value
/// @param[in]	d   scalar
/// @returns the lhs from where d is subtracted
template <typename T>
constexpr Vec3<T> operator-=(Vec3<T>& lhs, double d) noexcept
{
  lhs.x -= d;
  lhs.y -= d;
  lhs.z -= d;
  return lhs;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_VECTOR_H
